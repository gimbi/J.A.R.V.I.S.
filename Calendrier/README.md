Avec ce plugin je peux demander à JARVIS de consulter mon agenda et suivant l'heure qu'il est, il me donne l'agenda du jour ou bien du lendemain.<br/><br/>
<b>Fonctionnement</b> :<br/>
J'utilise un fichier sur mon Dropbox nommé "calendrier.txt". De cette manière je peux ajouter ou retirer des rendez-vous depuis n'importe où.<br/>
La syntaxe pour les rendez-vous doit être précise, actuellement ça se présente sous cette forme : "-28 11 2016-rdvA-4 12 2016-rdvB".<br/>
Ensuite je "split" les tirets entre les dates et les rendez-vous et je met tous ça dans un array() que je manipule par la suite.<br/>
Pour finir, si je demande à JARVIS de consulter l'agenda avant 16h, il me donne le rendez-vous du jour même. Si je le demande après 16h, il me donne le rendez-vous du lendemain.<br/><br/>
Comme vous pouvez le constater le fonctionnement est très simple. Pour ce qui est du nombre de rendez-vous par jour et l'utilisation de la condition suivante l'heure qu'il est, c'est pour mon utilisation personnelle mais on pourrait très bien ajouter plus de rendez-vous par jour, mettre d'autres options, ...
