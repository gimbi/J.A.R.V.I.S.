Avec ce plugin, je peux demander à JARVIS de déclencher mon scénario maison. Il y a quatres actions :
- JARVIS me donne une phrase de bienvenue.
- Il lance une musique.
- Je suis informé des rendez-vous du lendemain.
- Mon interface s'affiche sur le miroir.

Evidemment on pourrait ajouter d'autres actions en fonction des plugins déjà existant mais on pourrait aussi en créer d'autres directement dans le fichier home.js

