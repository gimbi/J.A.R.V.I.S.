<html>
	<head>
		<link href="http://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet" type="text/css"> 
		<script type="text/javascript" src="heure.js"></script>
		<link rel="stylesheet" type="text/css" href="http://localhost/jarvis/style.css">
	</head>

	<body>
		<div class="carre">
			<div id="sphere">
				<img src="sphere1.gif"/>
			</div>
		
			<div id="hr">
				<span id="heure"></span>
				<script type="text/javascript">window.onload = heure('heure');</script>
			</div>

			<div id="alim">
				<?php
					include("pourcentage_alim.php");
				?>
			</div>

			<div id="rappel">
				<?php
					include("rappel.php");
				?>
			</div>

			<div id="date">
				<?php
					include("date.php");
				?>
			</div>
		
			<div id="calendrier">
				<?php
					include("calendrier.php");
				?>
			</div>
		</div>
	</body>
</html>
