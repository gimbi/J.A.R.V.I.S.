<b>Fonctionnement</b> :<br/>
J'affiche diverses informations sur le miroir connecté :
- Date.
- Heure.
- Calendrier.
- Rappel.
- Stock alimentaire.
- Une sphere.gif (just for fun ^^)

Je vous détail ci-dessous les différents fichiers PHP :
- <b>matin.php</b> : Il s'agit du code principal pour l'interface. A l'intérieur, j'utilise des includes(); pour intégrer les "sous-codes" PHP.

- <b>calendrier.php</b> : Il va récupérer le contenu du fichier texte "calendrier.txt" situé dans ma Dropbox pour ensuite afficher mes rendez-vous
de manière ordonné.
J'utilise une Dropbox afin de pouvoir ajouter ou retirer des rendez-vous depuis n'importe où.

- <b>rappel.php</b> : Il va récupérer le contenu du fichier texte "rappel.txt" situé dans ma Dropbox pour ensuite afficher mes rappels
de manière ordonné.
J'utilise une Dropbox afin de pouvoir ajouter ou retirer des rendez-vous depuis n'importe où.

- <b>pourcentage_alim.php</b> : Un peu plus compliqué cette fois-ci. Le principe est le suivant; je créer une variable array(); contenant des aliments/objets prédéfinies.<br/>
Ex : $laitier = array('fromage','dessert','cremefraiche','chantilly','beurre','fromageblanc','gruillere','FIN');<br/><br/>
Je créer une variable contenant le même nombre d'aliments/objets.<br/>
Ex : $pourcentageLa = 7;<br/><br/>
J'ai créé un fichier texte nommé "laitier.txt" et positioné dans ma Dropbox j'ai remis, également, les même aliments/objets.<br/>
Ex : fromage<b>x</b> dessert cremefraiche chantilly beurre fromageblanc gruillere FIN<br/><br/>
Maintenant avec des conditions, je cherche à savoir si un aliment/objet contenu dans mon fichier "laitier.txt" est différent de ce qui se trouve dans ma variable array();<br/>
Pour ça, je met tous simplement un "x" à la fin de "fromage", par exemple, et si "fromage" est bien différent de "fromagex" alors je retire 1 à ma variable "$pourcentageLa".<br/><br/>
Pour finir j'effectue une opération afin d'obtenir un pourcentage de ce qu'il me reste dans mon stock alimentaire.<br/>
Ex : $result1 = round(($pourcentageLe * 100) / 7);<br/><br/>
Et j'attribue une couleur suivant ce pourcentage. Il ne reste plus qu'a exploiter la couleur pour l'afficher sous forme de tableau.

- <b>date.php</b> : Afficher la date du jour.

- <b>heure.js</b> : Afficher l'heure.

Voilà, j'espére avoir été suffisamment précis dans les explications. Evidemment il y aura quelques modifications à apportés de votre côté, notemment pour la taille de l'interface comparé à votre miroir.
