Il s'agit du plugin météo que vous pouvez retrouver dans le marketplace de SARAH ou directement <a href="https://github.com/JpEncausse/SARAH-Plugin-Meteo"><b>ici</b></a>

J'y ai apporté quelques modifications concernant les phrases. EN fonction du temps qu'il fait et de la température, JARVIS me donne une indication supplémentaire sur ce que je dois faire (ex: pense à prendre un parapluie. Prend une écharpe...).

Je vous met seulement le fichier meteo.js que j'ai modifié, le reste n'a pas été touché donc vous pouvez regarder ceux qui se trouvent dans le marketplace.

