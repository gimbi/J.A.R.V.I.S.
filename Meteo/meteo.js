﻿var action = function(data, callback, config, SARAH){ 

  // Retrieve config
  config = config.modules.meteo;
  if (!config.zip){
    console.log("Missing Meteo config");
    callback({'tts' : 'Zip code invalide'});
    return;
  }
  
  var url = 'http://www.meteo-france.mobi/ws/getDetail/france/'+(data.zip || config.zip)+'.json'
  var request = require('request');
  
  request({ 'uri' : url }, function (err, response, body){
  
    if (err || response.statusCode != 200) {
      callback({'tts': "Désolé david mais l'action a échoué"});
      return;
    }

    last = scrap(body, data.date || config.date);
    callback(last);
  });
}
exports.action = action;

// ==========================================
//  INIT
// ==========================================

var last = false;
exports.last = function(){  
  if (!last){
    action({}, function(){}, SARAH.ConfigManager.getConfig(), SARAH);
  }
  return last; 
};

var SARAH = false;
exports.init = function(bot){ SARAH = bot; }

// ==========================================
//  SCRAP
// ==========================================

var scrap = function(body, date){

  var meteo  = JSON.parse(body).result;
  var json   = { tts : 'Les prévision météo pour" '};
  var prev   = getPrevision(meteo, date);

  json.place = meteo.ville.nom;
  json.temp  = prev.temperatureMin + '°C';

  json.sun   = prev.description;
  
  json.tts  += json.place + ": ";
  json.tts  += day[prev.jour+'_'+prev.moment]+', ';
  json.tts  += json.sun + ', '
  
  if (prev.temperatureMin != prev.temperatureMax){
    json.tts  += 'température entre ' + prev.temperatureMax + ' et ' + prev.temperatureMin + ' degrés. ';
  } else {
    json.tts  += 'température de ' + prev.temperatureMax + ' degrés. ';
  } 
  if (prev.description == "Pluie"){
    json.tts += " N'oublie pas de prendre un parapluie si tu sort.";
  }
  else if (prev.description == "Averses"){
    json.tts += " N'oublie pas de prendre un parapluie si tu sort.";
  }
  else if (prev.description == "Rares averses"){
    json.tts += ' Il serait préférable de prendre un parapluie si tu sort.';
  }
  else if (prev.description == "Pluies éparses"){
    json.tts += ' Il serait préférable de prendre un parapluie si tu sort.';
  }
  else if ((prev.description == "Très nuageux") && (prev.temperatureMin <= "10°C")) {
    json.tts += " Peut être qu'une écharpe serait utile.";
  }
  else if (prev.description == "Brume") {
    json.tts += " Soit prudent si tu prend la voiture.";
  }
  else if ((prev.description == "Belles éclaircies") && (prev.temperatureMin <= "10°C")) {
    json.tts += " Le temps est idéale pour se promener, mais pense à te couvrir.";
  }
  else if ((prev.description == "Belles éclaircies") && (prev.temperatureMin >= "10°C")) {
    json.tts += " Le temps est idéale pour se promener.";
  }
  else if ((prev.description == "Eclaircies") && (prev.temperatureMin <= "10°C")) {
    json.tts += " Le temps est idéale pour se promener, mais pense à te couvrir.";
  }
  else if ((prev.description == "Eclaircies") && (prev.temperatureMin >= "10°C")) {
    json.tts += " Le temps est idéale pour se promener.";
  }
  else if ((prev.description == "Ensoleillé") && (prev.temperatureMin <= "10°C")) {
    json.tts += " Le temps est idéale pour se promener, mais pense à te couvrir.";
  }
  else if ((prev.description == "Ensoleillé") && (prev.temperatureMin >= "10°C")) {
    json.tts += " Le temps est idéale pour se promener.";
  }
  else if ((prev.description == "Nuit claire") && (prev.temperatureMin <= "10°C")) {
    json.tts += ' Temps idéale pour observer les étoiles, cependant pense à bien te couvrir.';
  }
  else if ((prev.description == "Nuit claire") && (prev.temperatureMin >= "10°C")) {
    json.tts += ' Temps idéale pour observer les étoiles.';
  }
  else if ((prev.description == "Quelques flocons") && (prev.temperatureMin <= "1°C")) {
    json.tts += " Soit prudent si tu prens la voiture, il y a peut être un risque de vergla.";
  }
  else if ((prev.description == "Brouillard Givrant") && (prev.temperatureMin <= "1°C")) {
    json.tts += " Soit prudent si tu prend la voiture, il y a peut être un risque de vergla.";
  }
  else if ((prev.description == "Brouillard Givrant") && (prev.temperatureMin >= "1°C")) {
    json.tts += " Soit prudent si tu prend la voiture.";
  }
  else if (prev.description == "Brouillard") {
    json.tts += " Soit prudent si tu prened la voiture.";
  }
  else if (prev.description == "Pluie orageuse") {
    json.tts += " Soit prudent si vous prend la voiture.";
  }
  return json;
}

var getPrevision = function(meteo, date){
  var prevision = meteo.previsions;
  
  if (date.length > 2){
    return prevision[date]; 
  }
  
  date = parseInt(date);
  for (var p in prevision){ if (date-- <= 0) return prevision[p];  }
}

var day = {
  '0_matin': 'ce matin', 
  '0_midi' : 'ce midi', 
  '0_soir' : 'ce soir', 
  '0_nuit' : 'cette nuit',
  '1_matin': 'demain matin', 
  '1_midi' : 'demain midi', 
  '1_soir' : 'demain soir', 
  '1_nuit' : 'la nuit prochaine',
  '2_matin': 'après demain matin', 
  '2_midi' : 'après demain midi', 
  '2_soir' : 'après demain soir', 
  '2_nuit' : 'dans 2 nuits'
}
