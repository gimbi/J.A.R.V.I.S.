# JARVIS

<b>Présentation</b> :

Il y a plus d'un an maintenant que j'ai découvert un projet nommé <a href="http://blog.encausse.net/sarah/"><b>S.A.R.A.H. de Jean-Philippe Encausse.</b></a>

Le principe de fonctionnement est de déclencher des actions via des requêtes HTTP en utilisant différents moyens de communications (reconnaissance vocale, gestuelle ou faciale, reconnaissance de QRCode, planification à l'aide d'un Google calendar ou pour finir à l'aide d’applications tiers). 
Ensuite ces requêtes HTTP déclenchent à leur tour des plugins sur le serveur NodeJS utilisé par S.A.R.A.H.
Pour finir les scripts contenus dans les plugins interagissent avec les objets connectés (montre, lampe HUE, box domotique, ...) que vous avez pu installer ou bien avec de l'open data (horaires de bus, météo, horaires de cinéma, ...).

De mon côté j'ai donc utilisé S.A.R.A.H. afin d'apporter mes propres modifications et donné vie à JARVIS. 

JARVIS se présente sous la forme un miroir connecté à reconnaissance vocale, pour fonctionner il utilise la version 4.0.0 de S.A.R.A.H., un système d'exploitation Windows 8.1 avec un core i5 et 8Go de RAM. C'est une des <a href="http://jpencausse.github.io/SARAH-Documentation/?page=faq#quelle-configuration-machine-"><b>configurations optimale</b></a> proposée par son créateur. Il a également besoin de XAMPP pour exécuter des pages php (pour mon plugin Interface entre autre) et Dropbox qui contient des fichiers texte en lien avec mes propres plugins (Course par exemple).

<b>Matériel</b> :

Concernant le miroir, je vous laisse lire <a href="http://monptitnuage.fr/index.php?article110/presentation-de-mon-miroir-connecte"><b>mon article</b></a> où je détaille toute la fabrication.

Pour finir, JARVIS a besoin d'un microphone afin qu'il puisse vous entendre et aussi d'un haut parleur/enceinte. Je dois vous dire que pour le micro c'est toute une histoire.
En effet, suivant l'utilisation que vous voulez faire de S.A.R.A.H. il vous faudra un micro suffisamment puissant et avec une bonne sensibilité, pourquoi ? Eh bien il y a deux principales raisons :
- Comme l'explique Jean-Philippe dans ses documentations, il arrive parfois qu'il y ai des faux positifs, c'est à dire que S.A.R.A.H. déclenche des actions à cause de la TV qui est trop forte et donc enregistre un mot qui serait dans un de vos plugins ou bien à cause de la musique, ou encore lorsqu'il y a trop de monde qui parle dans la même pièce.

- Plus les pièces de la maison/appartement sont grande plus le risque de faux positifs ou bien de perte au niveau du microphone est élevé. Il faut que le micro soit suffisemment puissant pour qu'il puisse vous entendre de loin, si c'est ce que vous recherchez, ou sinon il faudra parler à côté du micro.

Me concernant, mon miroir est positionné sur un mur dans la cuisine et assez proche de la TV, autant dire que j'ai quand même des faux positifs de temps en temps, mais ça passe. Aussi j'ai réfléchi à une solution pour remédier à ça :
- acheter une Kinect  : Jean-Philippe explique qu'étant donné que la Kinect utilise plusieurs micros, la reconnaissance vocale marche plus que bien. Mais vous pouvez quand même avoir de temps en temps des faux positifs, cela ne change rien.

- Utilisé une oreillette bluetooth avec une fonction de contrôle Muet. J'y pense depuis un moment et à mon avis c'est une solution qui pourrait fonctionner. Evidement, comme je l'ai dis tout à l'heure, cela dépend de l'utilisation que vous voulez faire de S.A.R.A.H.

Etant donné que j'ai plusieurs pièces assez grandes chez moi + un étage, j'aimerais pouvoir parler à JARVIS depuis n'importe où et non devoir me rendre tout le temps auprès du miroir.
L'idée de l'oreillette permettrai ceci, je la connecte en bluetooth à mon UC (par chance les UC d'aujourd'hui on en général le bluetooth de disponible) puis avec le contrôle Muet je peux éviter de déclencher des faux positifs. JARVIS écoutera ce que je dis uniquement si j'active le micro.

Mais quelle est la ou les faille(s) de cette solution. Première chose, le fait que le son passe uniquement dans l'oreillette. Lorsque vous parlerez à votre S.A.R.A.H., vous l'entendrez dans l'oreillette et non sur un haut parleur.
Je pense qu'il est possible de dissocier le micro de l'oreillette avec le haut parleur du PC mais je n'ai pas fais de recherches pour le moment.

Deuxième chose ? Le fait de devoir porter quelque chose à l'oreille. Vous ne seriez pas obligé de la porter tous=t le temps mais vous seriez quand même dépendant de ça. Maintenant, si on y pense, l'oreillette pourrais s'utiliser comme un micro sans fil tous simple. Si vous n'avez pas besoin d'écouter S.A.R.A.H. évidemment.

Bref, pour mon utilisation personnelle, l'oreillette est la première chose qui me paraît idéale. Pour le moment j'utilise un microphone de bureau tous simple mais je suis obligé de rester à côté du miroir pour que JARVIS m'entende. Je ferais une petite MAJ dans ce README dès que j'aurais commandé l'oreillette et testé, pour vous dire ce que ça donne.

<b>Plugins</b> :

Je vous met ci-dessous tous les plugins que j'utilise actuellement avec JARVIS. Seulement ceux que j'ai créés ou bien modifiés seront disponibles, pour les autres je l'ai utilisent tel quel, vous pouvez donc les récupérer directement dans le marketplace de SARAH :
- Calendrier (disponible).
- Interface (disponible).
- Home (disponible).
- Course (disponible).
- Meteo (récupéré dans le marketplace et modifié).
- WoL (non disponible).
- Time (plugin installé de base et aucune modifications apportées)

Pour avoir un détail de leur fonctionnement, j'ai ajouté un README pour chaque plugin.

Information complémentaire :
- Pour modifier le nom de SARAH en JARVIS, il suffit de vous rendre dans le dossier "Client" puis d'éditer "custom.ini" pour ensuite remplacer la ligne "name=SARAH" par "name=JARVIS".<br/><br/>
<b>ATTENTION</b> : ne pas remplacer le mot "Sarah" dans vos fichier .xml de grammaire.<br/>
<i>; Hot replace SARAH name<br/>
; dans les fichiers XML (utilisé pour la reconnaissance vocale) il faut utiliser le mot clé SARAH<br/>
;   cependant on peut vouloir changer le nom par défaut en quelque chose d'autre, par exemple en JARVIS<br/>
;   pour ce faire on va laisser "SARAH" dans les fichiers XML, et dans le fichier custom.ini on va changer<br/>
;   le SARAH ci-dessous en JARVIS, et ça va fonctionner<br/>
name=SARAH</i>

- Vous allez voir dans certains plugins des fautes d'orthographe pour les phrases que JARVIS doit dire, et c'est normal. Il faut savoir que la voix de Windows ne gére pas bien les différents mots de la langue française.<br/>
Par exemple, si vous voulez faire dire à votre S.A.R.A.H. (Tu vas bien ?), vous entendrez en réalité (Tu vas<b>se</b> bien ?).<br/>
Donc pour empêcher cette prononciation, il faut oublier vos cours de français et écrire les mots différemment.<br/>

Pour plus d'informations sur JARVIS : <a href="http://monptitnuage.fr/index.php?article110/presentation-de-mon-miroir-connecte"><b>Présentation de mon miroir connecté</b></a>
